﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ExchangeRates.Core.Data.Model;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Service.Rates;
using Xunit;

namespace ExchangeRates.UnitTests.Repository
{
    public class HistoryRepositoryTests
    {
        [Fact]
        public void GetFakeHistories()
        {
            var historiesMockRepository = new HistoryMockRepository();
            var histories =  historiesMockRepository.Get();

            Assert.NotEmpty(histories);
        }

        [Fact]
        public void GetFakeHistory_ById()
        {
            var historiesMockRepository = new HistoryMockRepository();
            var history = historiesMockRepository.Get(1);

            Assert.NotNull(history);
        }

        [Fact]
        public void InsertFakeHistory_ShouldReturnDifferentFromZeroEntityId()
        {
            var historiesMockRepository = new HistoryMockRepository();
            var newHistory = new History
            {
                Id = 0,
                CreateDateTime = DateTime.Now,
                CurrencyName = "USD",
                CurrencyValue = 1.1111,
                RateFrom = DateTime.Now
            };

            var insertedEntityId = historiesMockRepository.Insert(newHistory);


            Assert.True(insertedEntityId!=0);

        }

        [Fact]
        public void UpdateFakeHistory_ShouldReturnSameEntityId()
        {
            var historiesMockRepository = new HistoryMockRepository();
            var updateHistory = new History
            {
                Id = 2,
                CreateDateTime = DateTime.Now,
                CurrencyName = "USD",
                CurrencyValue = 1.1111,
                RateFrom = DateTime.Now
            };

            var updatedEntityId = historiesMockRepository.Update(updateHistory);


            Assert.True(updatedEntityId == updateHistory.Id);

        }

        [Fact]
        public void DeleteFakeHistory_ShouldReturnDifferentFromZeroEntityId()
        {
            var historiesMockRepository = new HistoryMockRepository();

            var entityToDelete = historiesMockRepository.Get(2);
          

            var deletedEntityId = historiesMockRepository.Delete(entityToDelete);


            Assert.True(deletedEntityId != 0);

        }


    }
}
