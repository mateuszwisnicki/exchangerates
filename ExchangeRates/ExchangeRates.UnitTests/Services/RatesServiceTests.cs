﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ExchangeRates.Core.Service.Rates;
using Xunit;

namespace ExchangeRates.UnitTests.Services
{
    public class RatesServiceTests
    {
        [Fact]
        public async Task GetFakeRatesTests()
        {
            var rateMockService = new RatesMockService();
            var rate = await rateMockService.GetRatesAsync();

            Assert.NotNull(rate);
        }

        [Fact]
        public async Task GetFakeTimeSeriesTests()
        {
            var rateMockService = new RatesMockService();
            var rate = await rateMockService.GetTimeSeriesAsync();

            Assert.NotNull(rate);
        }



    }
}
