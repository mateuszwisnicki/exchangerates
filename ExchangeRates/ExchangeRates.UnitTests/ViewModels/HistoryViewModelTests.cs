﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExchangeRates.Core.Data.Model;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Service.Rates;
using ExchangeRates.Core.Service.RequestProvider;
using ExchangeRates.Core.ViewModel.History;
using ExchangeRates.Core.ViewModel.Rates;
using Xunit;

namespace ExchangeRates.UnitTests.ViewModels
{
    public class HistoryViewModelTests
    {
        [Fact]
        public void HistoriesProperty_IsNull_WhenViewModelInstantiatedTest()
        {
            var historyMockRepository = new HistoryMockRepository();
            var historyViewModel = new HistoryViewModel(historyMockRepository);

            var histories = historyViewModel.Histories;

            Assert.Null(histories);
        }

        [Fact]
        public void HistoriesProperty_IsNotNull_AfterViewModelInitializationTest()
        {
            
            var historyMockRepository = new HistoryMockRepository();
            var historyViewModel = new HistoryViewModel(historyMockRepository);

            historyViewModel.Initialize(null);

            Assert.NotNull(historyViewModel.Histories);
        }



        [Fact]
        public void SettingHistoriesProperty_ShouldRaisePropertyChanged()
        {
            
            bool invoked = false;
            var historyMockRepository = new HistoryMockRepository();
            var historyViewModel = new HistoryViewModel(historyMockRepository);

            historyViewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("Histories"))
                    invoked = true;
            };

            historyViewModel.Initialize(null);
            historyViewModel.Histories = new ObservableCollection<History>();

            Assert.True(invoked);
        }


        [Fact]
        public void SelectedItemProperty_IsNull_WhenViewModelInstantiatedTest()
        {
            var historyMockRepository = new HistoryMockRepository();
            var historyViewModel = new HistoryViewModel(historyMockRepository);



            Assert.Null(historyViewModel.SelectedItem);
        }


        [Fact]
        public async Task DeleteCommand_SetProperties_RatesAndDownloadDataDateTime()
        {
            var historyMockRepository = new HistoryMockRepository();
            var historyViewModel = new HistoryViewModel(historyMockRepository);

            historyViewModel.Initialize(null);

            historyViewModel.SelectedItem = historyViewModel.Histories.First();

            historyViewModel.DeleteCommand.Execute(null);

            Assert.Null(historyViewModel.SelectedItem);
            

        }

    }
}
