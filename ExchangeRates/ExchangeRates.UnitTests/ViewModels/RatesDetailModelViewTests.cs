﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ExchangeRates.Core;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Model.Rates;
using ExchangeRates.Core.Service.Rates;
using ExchangeRates.Core.Service.RequestProvider;
using ExchangeRates.Core.ViewModel;
using ExchangeRates.Core.ViewModel.Rates;
using Xunit;

namespace ExchangeRates.UnitTests.ViewModels
{
    public class RatesDetailModelViewTests
    {
        [Fact]
        public void DefaultDaysToShowInDetails_IsZero_WhenViewModelInstantiatedTest()
        {
            
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            var defaultDaysToShowInDetails = ratesDetailModelView.DaysToDisplay;

            Assert.True(0==defaultDaysToShowInDetails);
        }

        [Fact]
        public async Task DefaultDaysToShowInDetails_IsNotZero_AfterViewModelInitializationTest()
        {
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            await ratesDetailModelView.InitializeAsync(null);

            var defaultDaysToShowInDetails = ratesDetailModelView.DaysToDisplay;

            Assert.True(0!= defaultDaysToShowInDetails);
        }


        [Fact]
        public void ChartProperty_IsNull_WhenViewModelInstantiatedTest()
        {
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);



            Assert.Null(ratesDetailModelView.Chart);
        }

        [Fact]
        public async Task ChartProperty_IsNotNull_AfterViewModelInitializationTest()
        {
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            await ratesDetailModelView.InitializeAsync(null);

            Assert.NotNull(ratesDetailModelView.Chart);
        }


        [Fact]
        public void TimeSeriesRatesProperty_IsNull_WhenViewModelInstantiatedTest()
        {
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);



            Assert.Null(ratesDetailModelView.TimeSeriesRates);
        }

        [Fact]
        public async Task TimeSeriesRatesProperty_IsNotNull_AfterViewModelInitializationTest()
        {
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            await ratesDetailModelView.InitializeAsync(null);

            Assert.NotNull(ratesDetailModelView.TimeSeriesRates);
        }


        [Fact]
        public void SelectedItemProperty_IsNull_WhenViewModelInstantiatedTest()
        {

            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            var selectedItem = ratesDetailModelView.SelectedItem;

            Assert.Null(selectedItem);
        }


        [Fact]
        public async Task SettingTimeSeriesRatesProperty_ShouldRaisePropertyChanged()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            bool invoked = false;
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            ratesDetailModelView.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("TimeSeriesRates"))
                    invoked = true;
            };
            
            await ratesDetailModelView.InitializeAsync(null);
            ratesDetailModelView.TimeSeriesRates = await ratesDetailModelView.GetTimeSeries();

            Assert.True(invoked);
        }



        [Fact]
        public async Task SettingDaysToDisplayProperty_ShouldRaisePropertyChanged()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            bool invoked = false;
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            ratesDetailModelView.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("DaysToDisplay"))
                    invoked = true;
            };

            await ratesDetailModelView.InitializeAsync(null);
            ratesDetailModelView.DaysToDisplay = GlobalSetting.Instance.GetDefaultDaysToShowInDetails(); 

            Assert.True(invoked);
        }

        [Fact]
        public void SaveSelectedListViewItemCommand_SelectedItemIsNull_SendsSelectItemAlertMessage()
        {
            bool messageReceived = false;
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            Xamarin.Forms.MessagingCenter.Subscribe<RatesDetailViewModel>(
                this, "SelectItemAlert", (sender) =>
                {
                    messageReceived = true;
                });

            ratesDetailModelView.SelectedItem = null;
            ratesDetailModelView.SaveSelectedListViewItemCommand.Execute(null);

            Assert.True(messageReceived);
        }


        [Fact]
        public void SaveSelectedListViewItemCommand_SelectedItemIsNotNull_SendsDeselectItemAlertMessage()
        {
            bool messageReceived = false;
            var ratesMockService = new RatesMockService();
            var historyMockRepository = new HistoryMockRepository();
            var ratesDetailModelView = new RatesDetailViewModel(ratesMockService, historyMockRepository);

            Xamarin.Forms.MessagingCenter.Subscribe<RatesDetailViewModel>(
                this, "DeselectItem", (sender) =>
                {
                    messageReceived = true;
                });

            ratesDetailModelView.SelectedItem = new TimeSeriesRates()
            {
                CurrencyCode = "USD",
                Date = DateTime.Now,
                Value = 1.0
            };
            ratesDetailModelView.SaveSelectedListViewItemCommand.Execute(null);

            Assert.True(messageReceived);
        }

    }
}
