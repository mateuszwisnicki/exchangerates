﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ExchangeRates.Core;
using ExchangeRates.Core.Service.Rates;
using ExchangeRates.Core.Service.RequestProvider;
using ExchangeRates.Core.ViewModel;
using ExchangeRates.Core.ViewModel.Rates;
using Xunit;

namespace ExchangeRates.UnitTests.ViewModels
{
    public class RatesModelViewTests
    {
        [Fact]
        public async Task GetLatestRates_LatestRatesUriIsEmpty_ThrowHttpRequestException()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            GlobalSetting.Instance.LatestRates = String.Empty;
            var ratesMockService = new RatesMockService();
            var ratesModelView = new RatesViewModel(ratesMockService);

             var act =  ratesModelView.GetLatestRates();

            await Assert.ThrowsAsync<HttpRequestExc>(()=>act);
            Assert.Contains("Incorrect uri", act.Exception.Message);


        }

        [Fact]
        public async Task GetLatestRates_LatestRatesUriIsNotEmpty_ThenNotNull()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            var ratesMockService = new RatesMockService();
            var ratesModelView = new RatesViewModel(ratesMockService);

            var act = await ratesModelView.GetLatestRates();

            Assert.NotNull(act);
        }


        [Fact]
        public void RatesProperty_IsNull_WhenViewModelInstantiatedTest()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            var ratesMockService = new RatesMockService();
            var ratesViewModel = new RatesViewModel(ratesMockService);

            var rates = ratesViewModel.Rates;

            Assert.Null(rates);
        }

        
        [Fact]
        public async Task RatesProperty_IsNotNull_AfterViewModelInitializationTest()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            var ratesMockService = new RatesMockService();
            var ratesViewModel = new RatesViewModel(ratesMockService);

            await ratesViewModel.InitializeAsync(null);

            Assert.NotNull(ratesViewModel.Rates);
        }


        [Fact]
        public void DownloadDataProperty_IsNull_WhenViewModelInstantiatedTest()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            var ratesMockService = new RatesMockService();
            var ratesViewModel = new RatesViewModel(ratesMockService);



            Assert.Null(ratesViewModel.DownloadDataDateTime);
        }

        [Fact]
        public async Task DownloadDataProperty_IsNotNull_AfterViewModelInitializationTest()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            var ratesMockService = new RatesMockService();
            var ratesViewModel = new RatesViewModel(ratesMockService);

            await ratesViewModel.InitializeAsync(null);

            Assert.NotNull(ratesViewModel.DownloadDataDateTime);
        }


        [Fact]
        public async Task SettingRatesProperty_ShouldRaisePropertyChanged()
        {
            bool invoked = false;
            var ratesMockService = new RatesMockService();
            var ratesViewModel = new RatesViewModel(ratesMockService);

            ratesViewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("Rates"))
                    invoked = true;
            };
            ratesViewModel.Rates = await ratesMockService.GetRatesAsync();
            await ratesViewModel.InitializeAsync(null);

            Assert.True(invoked);
        }

        [Fact]
        public void SettingDownloadDataDateTimeProperty_ShouldRaisePropertyChanged()
        {
            bool invoked = false;
            var ratesMockService = new RatesMockService();
            var ratesViewModel = new RatesViewModel(ratesMockService);

            ratesViewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("DownloadDataDateTime"))
                    invoked = true;
            };
            ratesViewModel.DownloadDataDateTime = DateTime.Now;
          

            Assert.True(invoked);
        }


        [Fact]
        public async Task RefreshCommand_SetProperties_RatesAndDownloadDataDateTime()
        {
            GlobalSetting.Instance.RefreshLatestRates();
            var ratesMockService = new RatesMockService();
            var ratesViewModel = new RatesViewModel(ratesMockService);

            await ratesViewModel.InitializeAsync(null);
            ratesViewModel.RefreshCommand.Execute(null);

            Assert.NotNull(ratesViewModel.DownloadDataDateTime);
            Assert.NotNull(ratesViewModel.Rates);
            
        }

    }
}
