﻿using System;
using System.Collections.Generic;
using System.IO;
using ExchangeRates.Core;
using ExchangeRates.Core.Data;
using SQLite;


[assembly: Xamarin.Forms.Dependency(typeof(ExchangeRates.Droid.Implementations.SQLite))]
namespace ExchangeRates.Droid.Implementations
{
    public class SQLite:ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, GlobalSetting.Instance.LocalDbName);
            var connection = new SQLiteConnection(path);
             
            return connection;
        }
    }
}