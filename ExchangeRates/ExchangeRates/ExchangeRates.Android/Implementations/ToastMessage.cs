﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Widget;
using ExchangeRates.Core;
using ExchangeRates.Core.Data;
using ExchangeRates.Core.ViewModel;
using ExchangeRates.Droid.Implementations;
using SQLite;


[assembly: Xamarin.Forms.Dependency(typeof(MessageAndroid))]
namespace ExchangeRates.Droid.Implementations
{
    public class MessageAndroid : IToastMessage
    {
        public void LongToast(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortToast(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}