﻿using System.Collections.ObjectModel;
using Autofac;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Model.Rates;
using ExchangeRates.Core.Service.History;
using ExchangeRates.Core.Service.Rates;
using ExchangeRates.Core.Service.RequestProvider;
using ExchangeRates.Core.ViewModel;
using ExchangeRates.Core.ViewModel.History;
using ExchangeRates.Core.ViewModel.Rates;
using ExchangeRates.Core.Views.Rates;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ExchangeRates.Core
{
    public partial class App : Application
    {
        public static IContainer Container { get; set; }

        public App()
        {
            InitializeComponent();
            Initialize();
            MainPage = new NavigationPage(new RatesPage()); 
        }

        protected override void OnStart()
        {
            
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


        public static void Initialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance<IRequestProvider>(new RequestProvider());
            builder.RegisterInstance<IRatesService>(new RatesService(new RequestProvider()));
            builder.RegisterInstance<IHistoryRepository>(new HistoryRepository());
            builder.RegisterInstance<IHistoryService>(new HistoryService(new HistoryRepository()));
            builder.RegisterType<RatesViewModel>();
            builder.RegisterType<RatesDetailViewModel>();
            builder.RegisterType<HistoryViewModel>();

            App.Container = builder.Build();

         }

      
    }
}
