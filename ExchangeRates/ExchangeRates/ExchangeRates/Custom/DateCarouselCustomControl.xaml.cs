﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExchangeRates.Core.Custom
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DateCarouselCustomControl : ContentView
	{
	    private static DateTime displayDate;

	    public static readonly BindableProperty DateProperty = BindableProperty.Create(
	        propertyName: "Date",
	        returnType: typeof(string),
	        declaringType: typeof(DateCarouselCustomControl),
	        defaultValue: "",
	        defaultBindingMode: BindingMode.TwoWay,
	        propertyChanged: DatePropertyChanged);

	    public string Date
        {
	        get { return base.GetValue(DateProperty).ToString(); }
	        set { base.SetValue(DateProperty, value); }
	    }

	    private static void DatePropertyChanged(BindableObject bindable, object oldValue, object newValue)
	    {
	        var control = (DateCarouselCustomControl)bindable;
            control.Label.Text = newValue.ToString();
	        displayDate = DateTime.Parse(newValue.ToString(), CultureInfo.InvariantCulture);
	    }


        public DateCarouselCustomControl ()
		{
			InitializeComponent();
		    displayDate = DateTime.Now;

		}

	    private void OnRightTapped(object sender, EventArgs e)
	    {
	        displayDate = displayDate.AddDays(1);

            Label.Text = displayDate.ToString("g");
	    }

	    private void OnLeftTapped(object sender, EventArgs e)
	    {
	        displayDate = displayDate.AddDays(-1);

            Label.Text = displayDate.ToString("g");
        }
	}
}