﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;


namespace ExchangeRates.Core.Data
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
