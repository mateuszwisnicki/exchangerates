﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;


namespace ExchangeRates.Core.Data.Model
{
    [Table("History")]
    public class History
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public double CurrencyValue { get; set; }
        public string CurrencyName { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime RateFrom { get; set; }

    }
}
