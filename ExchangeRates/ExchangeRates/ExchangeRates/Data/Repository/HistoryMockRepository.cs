﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ExchangeRates.Core.Data.Model;

namespace ExchangeRates.Core.Data.Repository
{
    public class HistoryMockRepository:IHistoryRepository
    {
        private List<History> _histories = new List<History>()
        {
            new History() { CreateDateTime = DateTime.Now, CurrencyName = "USD", CurrencyValue = 1.234, Id = 1, RateFrom = DateTime.Now},
            new History() { CreateDateTime = DateTime.Now, CurrencyName = "USD", CurrencyValue = 1.254, Id = 2, RateFrom = DateTime.Now},
            new History() { CreateDateTime = DateTime.Now, CurrencyName = "CAD", CurrencyValue = 1.254, Id = 3, RateFrom = DateTime.Now},
            new History() { CreateDateTime = DateTime.Now, CurrencyName = "USD", CurrencyValue = 1.234, Id = 4, RateFrom = DateTime.Now},
            new History() { CreateDateTime = DateTime.Now, CurrencyName = "CAD", CurrencyValue = 1.344, Id = 5, RateFrom = DateTime.Now},
        };

        public List<History> Get()
        {
            return _histories;
        }

        public History Get(int id)
        {
            return _histories.FirstOrDefault(d => d.Id == id);
        }

   
        public int Insert(History entity)
        {
             entity.Id = _histories.Max(d => d.Id);
            _histories.Add(entity);
            return _histories.Max(d => d.Id) + 1;
        }

        public int Update(History entity)
        {
            var editEntity = _histories.Find(d => d.Id == entity.Id);
            
            editEntity.CurrencyValue = entity.CurrencyValue;
            editEntity.RateFrom = entity.RateFrom;
            editEntity.CurrencyName = entity.CurrencyName;

            return editEntity.Id;
        }

        public int Delete(History entity)
        {
            
            var toRemoveEntity = _histories.Find(d => d.Id == entity.Id);
            _histories.Remove(toRemoveEntity);
            
            return entity.Id;
        }

        public void Initialize()
        {
          
        }
    }
}
