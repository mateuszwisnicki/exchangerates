﻿using System;
using System.Collections.Generic;
using System.Text;
using ExchangeRates.Core.Data.Model;

namespace ExchangeRates.Core.Data.Repository
{
    public interface IHistoryRepository : IRepository<History>
    {
    }
}
