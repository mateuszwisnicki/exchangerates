﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace ExchangeRates.Core.Data.Repository
{
    public interface IRepository<T> where T : class, new()
    {
        List<T> Get();
        T Get(int id);
        int Insert(T entity);
        int Update(T entity);
        int Delete(T entity);
        void Initialize();
    }
}
