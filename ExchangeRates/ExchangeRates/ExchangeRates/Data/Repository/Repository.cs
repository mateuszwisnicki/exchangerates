﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;

namespace ExchangeRates.Core.Data.Repository
{
    public class SqLiteDatabase
    {
        public static SQLiteConnection GetConnection()
        {
            return  DependencyService.Get<ISQLite>().GetConnection();
        }
    }

    public class Repository<T> : IRepository<T> where T : class, new()
    {
        private SQLiteConnection _db;

        public Repository()
        {
            Initialize();
        }
        
        public void Initialize()
        {
            _db = SqLiteDatabase.GetConnection();
            
            _db.CreateTable<T>();
        }

        public List<T> Get() =>
            _db.Table<T>().ToList();

        public T Get(int id) =>
            _db.Find<T>(id);

        
        public int Insert(T entity) =>
            _db.Insert(entity);

        public int Update(T entity) =>
            _db.Update(entity);

        public int Delete(T entity) =>
            _db.Delete(entity);
    }
}
