﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeRates.Core
{
    public class GlobalSetting
    {
        private const string ApiKey = "92efdf738292c30b62f666e6b2160c7e";
        private const string DefaultEndpoint = "http://data.fixer.io/api";
        public const int DefaultDaysToShowInDetails = 30;
        
        //keys
        private const string _startDateKey = "SDATE";
        private const string _endDateKey = "EDATE";
        private const string _localDbName = "exchangerates.db";

        public GlobalSetting()
        {
            CreateEndpoints(DefaultEndpoint, ApiKey);
        }

        public static GlobalSetting Instance { get; } = new GlobalSetting();

        public string LatestRates { get; set; }

        public string TimeSeries { get; set; }

        public string StartDateToken { get; set; }

        public string EndDateToken { get; set; }

        public string LocalDbName { get; set; }

        public void RefreshLatestRates()
        {
            CreateEndpoints(DefaultEndpoint, ApiKey);
        }

        public int GetDefaultDaysToShowInDetails()
        {
            return DefaultDaysToShowInDetails;
        }
      

        private void CreateEndpoints(string endpoint,string apiKey)
        {
            LatestRates = $"{endpoint}/latest?access_key={apiKey}";
            TimeSeries = $"{endpoint}/latest?access_key={apiKey}&start_date={_startDateKey}&end_date={_endDateKey}";
            StartDateToken = _startDateKey;
            EndDateToken = _endDateKey;
            LocalDbName = _localDbName;
        }
    }
}
