﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeRates.Core.Model.Rates
{
    public class LatesRates
    {
        public string Name { get; set; }
        public double Value { get; set; }

        
        public string ItemText
        {
            get { return $"Currency: {this.Name} Rate; {this.Value.ToString("F")}"; }
        }
    }
}
