﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ExchangeRates.Core.Model.Rates
{
    public class LatestRatesRoot
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "timestamp")]
        public int Timestamp { get; set; }

        [JsonProperty(PropertyName = " @base")]
        public string Base { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "rates")]
        public Rates Rates { get; set; }
    }
}
