﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeRates.Core.Model.Rates
{
    public class TimeSeriesRates
    {
        public string CurrencyCode { get; set; }
        public DateTime Date { get; set; }
        public double Value { get; set; }
    }
}
