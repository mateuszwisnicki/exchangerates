﻿using System.Collections.ObjectModel;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Helpers;

namespace ExchangeRates.Core.Service.History
{
    public class HistoryService : IHistoryService
    {
        private readonly IHistoryRepository _historyRepository;

        public HistoryService(IHistoryRepository historyRepository)
        {
            _historyRepository = historyRepository;
        }

        public ObservableCollection<Data.Model.History> GetHistoryRates()
        {
            return _historyRepository.Get().ToObservableCollection();
        }

        public int AddNewHistory(Data.Model.History item)
        {
            return _historyRepository.Insert(item);
        }
    }
}
