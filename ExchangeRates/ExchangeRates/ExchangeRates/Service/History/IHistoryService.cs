﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ExchangeRates.Core.Service.History
{
    public interface IHistoryService
    {
        ObservableCollection<Data.Model.History> GetHistoryRates();
        int AddNewHistory(Data.Model.History item);
    }
}
