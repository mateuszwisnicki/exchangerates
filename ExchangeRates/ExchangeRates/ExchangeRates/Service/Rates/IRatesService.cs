﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ExchangeRates.Core.Model.Rates;

namespace ExchangeRates.Core.Service.Rates
{
    public interface IRatesService
    {
        Task<ObservableCollection<LatesRates>> GetRatesAsync();
        Task<ObservableCollection<TimeSeriesRates>> GetTimeSeriesAsync();
    }
}
