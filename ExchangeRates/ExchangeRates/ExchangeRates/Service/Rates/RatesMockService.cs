﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Threading.Tasks;
using ExchangeRates.Core.Model.Rates;
using ExchangeRates.Core.Service.RequestProvider;

namespace ExchangeRates.Core.Service.Rates
{
    public class RatesMockService : IRatesService
    {
        private ObservableCollection<LatesRates> _latestRates = new ObservableCollection<LatesRates>()
        {
            new LatesRates() {Name = "PLN", Value = 3.1508},
            new LatesRates() {Name = "USD", Value = 1.1508},
            new LatesRates() {Name = "CAD", Value = 1.2308}
        };

        private ObservableCollection<TimeSeriesRates> _timeeSeries = new ObservableCollection<TimeSeriesRates>()
        {
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-30), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-29), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-28), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-27), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-26), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-25), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-24), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-23), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-22), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-21), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-20), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-19), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-18), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-17), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-16), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-15), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-14), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-13), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-12), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-11), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-10), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-9), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-8), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-7), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-6), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-5), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-4), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-3), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-2), Value = 3.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-1), Value = 3.1508, CurrencyCode = "USD"},
           
          
        };


        public async Task<ObservableCollection<LatesRates>> GetRatesAsync()
        {

            await Task.Delay(1);

            var uri = GlobalSetting.Instance.LatestRates;

            if (string.IsNullOrEmpty(uri))
            {
                throw new HttpRequestExc(HttpStatusCode.MethodNotAllowed, @"Incorrect uri");
            }
           

            return _latestRates;
        }

        public async Task<ObservableCollection<TimeSeriesRates>> GetTimeSeriesAsync()
        {
            await Task.Delay(1);
            var uri = GlobalSetting.Instance.TimeSeries;


            var dayToGet = GlobalSetting.Instance.GetDefaultDaysToShowInDetails();

            uri = uri.Replace(GlobalSetting.Instance.StartDateToken,
                    DateTime.Now.AddDays(dayToGet * -1).ToString("YYYY-MM-DD"))
                .Replace(GlobalSetting.Instance.EndDateToken, DateTime.Now.ToString("YYYY-MM-DD"));

            if (string.IsNullOrEmpty(uri))
            {
                throw new HttpRequestExc(HttpStatusCode.MethodNotAllowed, @"Incorrect uri");
            }

            return _timeeSeries;
        }
    }
}
