﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ExchangeRates.Core.Data.Model;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Helpers;
using ExchangeRates.Core.Model.Rates;
using ExchangeRates.Core.Service.RequestProvider;
using ExchangeRates.Core.ViewModel;

namespace ExchangeRates.Core.Service.Rates
{
    public class RatesService : IRatesService
    {
        private readonly IRequestProvider _requestProvider;
        private readonly IHistoryRepository _historyRepository;
        

        public RatesService(IRequestProvider requestProvider)
        {
            _requestProvider = requestProvider;
           
        }

        public async Task<ObservableCollection<LatesRates>> GetRatesAsync()
        {
            var uri = GlobalSetting.Instance.LatestRates;

            if (string.IsNullOrEmpty(uri))
            {
                throw new HttpRequestExc(HttpStatusCode.MethodNotAllowed, @"Incorrect uri");
            }

            IList<LatesRates> latesRateses = new List<LatesRates>();

            var rootRates = await _requestProvider.GetAsync<LatestRatesRoot>(uri);

            foreach (var propertyInfo in rootRates.Rates.GetType().GetProperties())
            {
                var name = propertyInfo.Name;
                var value = propertyInfo.GetValue(rootRates.Rates, null);

                latesRateses.Add(new LatesRates() {Name = name, Value = (double) value});
            }


            return latesRateses.ToObservableCollection();
        }


        public async Task<ObservableCollection<TimeSeriesRates>> GetTimeSeriesAsync()
        {
            var uri = GlobalSetting.Instance.TimeSeries;
           

            var dayToGet = GlobalSetting.Instance.GetDefaultDaysToShowInDetails();

            uri = uri.Replace(GlobalSetting.Instance.StartDateToken,
                    DateTime.Now.AddDays(dayToGet * -1).ToString("YYYY-MM-DD"))
                .Replace(GlobalSetting.Instance.EndDateToken, DateTime.Now.ToString("YYYY-MM-DD"));

            if (string.IsNullOrEmpty(uri))
            {
                throw new HttpRequestExc(HttpStatusCode.MethodNotAllowed, @"Incorrect uri");
            }

            //todo
            //request to api

            //fake data
            var timeSeries  = new ObservableCollection<TimeSeriesRates>(){

            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-30), Value = 1.3508 , CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-29), Value = 1.4508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-28), Value = 3.3508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-27), Value = 3.6508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-26), Value = 2.2508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-25), Value = 1.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-24), Value = 4.2508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-23), Value = 5.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-22), Value = 1.2508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-21), Value = 2.4508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-20), Value = 2.2208, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-19), Value = 1.8508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-18), Value = 1.1508, CurrencyCode = "USD"},
            new TimeSeriesRates() {Date = DateTime.Now.AddDays(-17), Value = 2.1508, CurrencyCode = "USD"}
           ,};


            return timeSeries;
        }

        
    }
}
