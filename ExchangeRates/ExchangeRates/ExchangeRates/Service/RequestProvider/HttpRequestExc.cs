﻿using System;
using System.Net.Http;

namespace ExchangeRates.Core.Service.RequestProvider
{
    public class HttpRequestExc : System.Net.Http.HttpRequestException
    {
        public System.Net.HttpStatusCode HttpCode { get; }
        public HttpRequestExc(System.Net.HttpStatusCode code) : this(code, null, null)
        {
        }

        public HttpRequestExc(System.Net.HttpStatusCode code, string message) : this(code, message, null)
        {
        }

        public HttpRequestExc(System.Net.HttpStatusCode code, string message, Exception inner) : base(message,
            inner)
        {
            HttpCode = code;
        }

    }
}