﻿using System.Threading.Tasks;

namespace ExchangeRates.Core.Service.RequestProvider
{
    public interface IRequestProvider
    {
        Task<TResult> GetAsync<TResult>(string uri);
        
       
    }
}
