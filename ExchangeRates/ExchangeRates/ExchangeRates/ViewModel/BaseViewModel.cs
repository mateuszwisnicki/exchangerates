﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace ExchangeRates.Core.ViewModel
{
    public class BaseViewModel : IToastMessage, INotifyPropertyChanged
    {
        public void LongToast(string message)
        {
            DependencyService.Get<IToastMessage>().LongToast(message);
        }

        public void ShortToast(string message)
        {
            DependencyService.Get<IToastMessage>().ShortToast(message);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
