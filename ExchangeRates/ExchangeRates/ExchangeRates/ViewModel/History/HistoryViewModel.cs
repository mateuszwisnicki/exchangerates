﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Helpers;
using ExchangeRates.Core.Model.Rates;
using Xamarin.Forms;

namespace ExchangeRates.Core.ViewModel.History
{
    public class HistoryViewModel : BaseViewModel
    {
        private readonly IHistoryRepository _historyRepository;
        public HistoryViewModel(IHistoryRepository historyRepository)
        {
            _historyRepository = historyRepository;
        }

        private ObservableCollection<Data.Model.History> GetHistories()
        {
            return _historyRepository.Get().ToObservableCollection();
        }

        private ObservableCollection<Data.Model.History> _histories;

        public ObservableCollection<Data.Model.History> Histories
        {
            get { return _histories; }
            set
            {
                _histories = value;
                NotifyPropertyChanged("Histories");
            }
        }

        private Data.Model.History _selectedItem;

        public Data.Model.History SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
               
            }
        }


        public ICommand DeleteCommand => new Command(() =>  RemoveHistoryItem());

        private void RemoveHistoryItem()
        {
            if (_selectedItem == null)
            {
                MessagingCenter.Send(this, "SelectItemAlert");
            }
            else
            {
                _historyRepository.Delete(_selectedItem);
                var isDeleted = _histories.Remove(_selectedItem);
                //ShortToast($"{_selectedItem.CurrencyName} from {_selectedItem.RateFrom} has been deleted");

                if (isDeleted)
                {
                    _selectedItem = null;
                }
            }

            
        }

        public void Initialize(object navigationData)
        {

            _histories =  GetHistories();

        }
    }
}
