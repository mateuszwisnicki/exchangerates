﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeRates.Core.ViewModel
{
    public interface IToastMessage
    {
        void LongToast(string message);
        void ShortToast(string message);
    }
}
