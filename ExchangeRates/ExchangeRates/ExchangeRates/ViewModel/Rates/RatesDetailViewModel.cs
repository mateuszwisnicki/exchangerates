﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using ExchangeRates.Core.Data.Model;
using ExchangeRates.Core.Data.Repository;
using ExchangeRates.Core.Model.Rates;
using ExchangeRates.Core.Service.Rates;
using ExchangeRates.Core.Views.History;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;

namespace ExchangeRates.Core.ViewModel.Rates
{
    public class RatesDetailViewModel : BaseViewModel
    {
        private IRatesService _ratesService;
        private IHistoryRepository _historyRepository;
        public RatesDetailViewModel(IRatesService ratesService, IHistoryRepository historyRepositorys)
        {
            _ratesService = ratesService;
            _historyRepository = historyRepositorys;
        }
        
        public async Task<ObservableCollection<TimeSeriesRates>> GetTimeSeries()
        {
           
            return await _ratesService.GetTimeSeriesAsync();
        }

        private void SetChartEntries()
        {
            var chartEntries = _timeSeriesRates.Select(d => new Microcharts.Entry((float) d.Value)
            {
                Color = SkiaSharp.SKColor.Parse("#FF1493"),
                Label = d.Date.ToString("d"),
                ValueLabel = String.Empty
            }).ToList();

            _chart = new LineChart();
            _chart.Entries = chartEntries;
            _chart.BackgroundColor = SKColors.Transparent;
        }

        private int _daysToDisplay;

        public int DaysToDisplay
        {
            get { return _daysToDisplay; }
            set
            {
                _daysToDisplay = value;
                NotifyPropertyChanged("DaysToDisplay");
            }
        }

        private ObservableCollection<TimeSeriesRates> _timeSeriesRates;

        public ObservableCollection<TimeSeriesRates> TimeSeriesRates
        {
            get { return _timeSeriesRates; }
            set
            {
                _timeSeriesRates = value;
                NotifyPropertyChanged("TimeSeriesRates");
            }
        }
        
        private LineChart _chart;

        public LineChart Chart
        {
            get { return _chart; }
        }

        private TimeSeriesRates _selectedItem;

        public TimeSeriesRates SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                
            }
        }

     
        public ICommand GoToHistoryPageCommand => new Command(async()=> await ShowHistoryPage());

        public ICommand SaveSelectedListViewItemCommand => new Command(async()=> await SaveSelecedListViewItem());


        private async Task ShowHistoryPage()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new HistoryPage());
        }

        private async Task SaveSelecedListViewItem()
        {
            if (_selectedItem == null)
            {
                MessagingCenter.Send(this, "SelectItemAlert");
            }
            else
            {
                var newHistoryItem = new Data.Model.History()
                {
                    Id = 0,
                    CreateDateTime = DateTime.Now,
                    CurrencyValue = _selectedItem.Value,
                    RateFrom = _selectedItem.Date,
                    CurrencyName = _selectedItem.CurrencyCode
                    
                    
                };

                var id =  _historyRepository.Insert(newHistoryItem);

                if (id != 0)
                {
                    MessagingCenter.Send(this, "DeselectItem");
                    //ShortToast("Data saved");
                }

            }
        }



        public async Task InitializeAsync(object navigationData)
        {

            _daysToDisplay = GlobalSetting.Instance.GetDefaultDaysToShowInDetails();

           _timeSeriesRates =await GetTimeSeries();

            SetChartEntries();
        }

       
    }
}
