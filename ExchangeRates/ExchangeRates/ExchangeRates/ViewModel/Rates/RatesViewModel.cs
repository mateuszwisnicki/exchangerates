﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using ExchangeRates.Core.Model.Rates;
using ExchangeRates.Core.Service.Rates;
using Xamarin.Forms;

namespace ExchangeRates.Core.ViewModel.Rates
{
	public class RatesViewModel : BaseViewModel
    {
	    private IRatesService _ratesService;
	    public  RatesViewModel(IRatesService ratesService)
	    {
           
	        _ratesService = ratesService;
	        


	    }
        
        public async Task<ObservableCollection<LatesRates>> GetLatestRates()
	    {
	        DownloadDataDateTime = DateTime.Now;

            return await _ratesService.GetRatesAsync();
        }



	    private ObservableCollection<LatesRates> _rates;


	    public ObservableCollection<LatesRates> Rates
	    {
	        get { return _rates; }
	        set
	        {
	            _rates = value;
	            NotifyPropertyChanged("Rates");
	        }
	    }


      

        private DateTime? _downloadDataDateTime = null;

        public DateTime? DownloadDataDateTime
        {
            get { return _downloadDataDateTime; }
            set
            {
                _downloadDataDateTime = value;
                NotifyPropertyChanged("DownloadDataDateTime");
            }
        }

        public ICommand RefreshCommand => new Command(async () =>await Refresh());

        private async Task Refresh()
        {

             Rates =  await GetLatestRates();
           
             ShortToast("Refresh finished");
        }


        public async Task InitializeAsync(object navigationData)
	    {

	        _rates = await GetLatestRates();

	    }
    }
}