﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ExchangeRates.Core.ViewModel.History;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExchangeRates.Core.Views.History
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPage : ContentPage
    {
        private HistoryViewModel _viewModel = null;

        public HistoryPage()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<HistoryViewModel>(this, "SelectItemAlert",
                (sender) => { DisplayAlert("Warning", "Please select row before save", "OK"); });

        }


        protected override async void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                IsBusy = true;


                using (var scope = App.Container.BeginLifetimeScope())
                {
                    _viewModel = App.Container.Resolve<HistoryViewModel>();
                }

                _viewModel.Initialize(null);

                BindingContext = _viewModel;


                IsBusy = false;
            }


            catch (Exception ex)
            {
                await DisplayAlert("Warning", "Can't display data", "OK");
            }
        }
    }
}