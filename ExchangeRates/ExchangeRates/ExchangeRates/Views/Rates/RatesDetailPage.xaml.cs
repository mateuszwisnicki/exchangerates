﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ExchangeRates.Core.ViewModel;
using ExchangeRates.Core.ViewModel.Rates;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExchangeRates.Core.Views.Rates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RatesDetailPage : ContentPage
    {
        private string _currencyCode;
        private RatesDetailViewModel _ratesDetailViewModel;

        public RatesDetailPage(string currencyCode)
        {
            _currencyCode = currencyCode;

            MessagingCenter.Subscribe<RatesDetailViewModel>(this, "SelectItemAlert",
                (sender) => { DisplayAlert("Warning", "Please select row before save", "OK"); });


            MessagingCenter.Subscribe<RatesDetailViewModel>(this, "DeselectItem",
                (sender) => { Rates.SelectedItem = null; });


            InitializeComponent();
        }



        protected override async void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                IsBusy = true;

                using (var scope = App.Container.BeginLifetimeScope())
                {
                    _ratesDetailViewModel = App.Container.Resolve<RatesDetailViewModel>();
                }

                Title = _currencyCode;

                await _ratesDetailViewModel.InitializeAsync(null);

                BindingContext = _ratesDetailViewModel;


                IsBusy = false;
            }


            catch (Exception ex)
            {
                await DisplayAlert("Warning", "Can't display data", "OK");
            }
        }




    }
}