﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using ExchangeRates.Core.Model.Rates;
using ExchangeRates.Core.Service.Rates;
using ExchangeRates.Core.ViewModel;
using ExchangeRates.Core.ViewModel.Rates;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExchangeRates.Core.Views.Rates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RatesPage : ContentPage
    {
        private RatesViewModel _viewModel = null;
        public RatesPage()
        {
            InitializeComponent();
        }

        protected override async void OnDisappearing()
        {
            base.OnDisappearing();

      
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                IsBusy = true;

                

                using (var scope = App.Container.BeginLifetimeScope())
                {
                    _viewModel = App.Container.Resolve<RatesViewModel>();
                }

                await _viewModel.InitializeAsync(null);

                BindingContext = _viewModel;

               

                IsBusy = false;
            }


            catch (Exception ex)
            {
                await DisplayAlert("Warning", "Can't display data", "OK");
            }
        }


     

        private void OnCurrencyChange(object sender, EventArgs e)
        {
            DisplayAlert("Warning", "You can't change base currency", "OK");
        }

      

        private void Rates_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var ratesSelectedItem = e.SelectedItem as LatesRates;
            Navigation.PushAsync(new RatesDetailPage(ratesSelectedItem.Name));

        }
    }
}
